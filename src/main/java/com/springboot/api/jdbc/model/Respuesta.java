package com.springboot.api.jdbc.model;

import java.util.List;

public class Respuesta {
	private String codigo_servicio;
	private List<Cliente> clientes;
	private Cliente cliente;
	private String descripcion;
	
	public Respuesta() {
		
	}
	public String getCodigo_servicio() {
		return codigo_servicio;
	}
	public void setCodigo_servicio(String codigo_servicio) {
		this.codigo_servicio = codigo_servicio;
	}
	public List<Cliente> getClientes() {
		return clientes;
	}
	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
