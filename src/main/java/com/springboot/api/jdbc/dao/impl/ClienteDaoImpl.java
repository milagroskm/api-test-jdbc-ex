package com.springboot.api.jdbc.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.api.jdbc.dao.ClienteDao;
import com.springboot.api.jdbc.model.Cliente;
import com.springboot.api.jdbc.model.Respuesta;
import com.springboot.api.jdbc.rowmapper.ClienteRowMapper;

@Repository
public class ClienteDaoImpl extends JdbcDaoSupport implements ClienteDao {

	public ClienteDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	@Override
	public Respuesta getAllClientes() {
		
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		Respuesta respuesta = new Respuesta();
		
		String sql = " SELECT id, nombres, apellido_pat, apellido_mat, sexo, direccion, estado\n" + 
				" FROM microservicios.cliente";
		
		try {
			
			RowMapper<Cliente> clienteRow = new ClienteRowMapper();
			listaClientes = getJdbcTemplate().query(sql, clienteRow);
			logger.debug("Se han listado "+listaClientes.size()+" clientes");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
			//respuesta.setCodigo_servicio(e.getMessage());
		}
		respuesta.setCodigo_servicio("0000");
		respuesta.setClientes(listaClientes);
		return respuesta;
	}

	@Override
	public Respuesta getCliente(Integer id) {
		Cliente cliente = new Cliente();	
		Respuesta respuesta = new Respuesta();
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		
		String sql = " SELECT id, nombres, apellido_pat, apellido_mat, sexo, direccion, estado\n" + 
				" FROM microservicios.cliente where id='"+id+"'";
				
		try {
			
			RowMapper<Cliente> clienteRow = new ClienteRowMapper();
			listaClientes = getJdbcTemplate().query(sql, clienteRow);
			
			cliente = listaClientes.get(0);
			
			logger.debug("Se ha traido a la cliente "+listaClientes.get(0).toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		respuesta.setCodigo_servicio("0000");
		respuesta.setCliente(cliente);
		return respuesta;
	}

	@Override
	public Respuesta saveCliente(Cliente cliente) {
		
		Respuesta respuesta = new Respuesta();
		String sql = "insert into microservicios.cliente (nombres, apellido_pat, apellido_mat, sexo, direccion, estado) "  
				+ "values (?, ?, ?, ?, ?, ?);";
		
		Object[] params = { cliente.getNombres(), cliente.getApellido_pat(), cliente.getApellido_mat(), cliente.getSexo(), cliente.getDireccion(), cliente.getEstado()};
		int[] tipos = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado a la cliente "+cliente.toString());
			respuesta.setCodigo_servicio("0000");
			respuesta.setDescripcion("Cliente registrado con éxito");
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			respuesta.setCodigo_servicio("1111");
			respuesta.setDescripcion("ERROR: No se registró el cliente.");
		}
		return respuesta;

	}

	@Override
	public Respuesta deleteCliente(Integer id) {
		
		Respuesta respuesta = new Respuesta();
		int regeliminados = 0;		
		String sql = " delete from cliente where id ='"+id+"'";		
		try {			
			regeliminados = getJdbcTemplate().update(sql);
			logger.debug("Se han eliminado "+regeliminados+" cliente con id = "+id);
			respuesta.setCodigo_servicio("0000");
			respuesta.setDescripcion("Cliente eliminado con éxito.");
		} catch (Exception e) {			
			logger.error(e.getMessage());
			respuesta.setCodigo_servicio("1111");
			respuesta.setDescripcion("ERROR: No se eliminó el cliente.");
		}
		return respuesta;
	}

}
